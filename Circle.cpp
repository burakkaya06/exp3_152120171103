/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include <iostream>
#include "Circle.h"
using namespace std;
namespace Obj {
Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	this->r = r; 
}
void Circle::setR(int r) { 
	this->r = r;
}
double Circle::getR()const{
	return r;
}

double Circle::calculateCircumference()const{
	return PI * r * r;
}

double Circle::calculateArea(){
	return PI * r * 2;
}
bool Circle::equals(Circle other) {
	if (this->r == other.getR())
		return true;
	else
		return false;
}
}
